#include <time.h>
#include <stdlib.h>
#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

/* Extrait des pages Info de tar */

struct posix_header
{                              /* byte offset */
  char name[100];               /*   0 */
  char mode[8];                 /* 100 */
  char uid[8];                  /* 108 */
  char gid[8];                  /* 116 */
  char size[12];                /* 124 */
  char mtime[12];               /* 136 */
  char chksum[8];               /* 148 */
  char typeflag;                /* 156 */
  char linkname[100];           /* 157 */
  char magic[6];                /* 257 */
  char version[2];              /* 263 */
  char uname[32];               /* 265 */
  char gname[32];               /* 297 */
  char devmajor[8];             /* 329 */
  char devminor[8];             /* 337 */
  char prefix[155];             /* 345 */
                                /* 500 */
};

#define TMAGIC   "ustar"        /* ustar and a null */
#define TMAGLEN  6
#define TVERSION "00"           /* 00 and no null */
#define TVERSLEN 2

/* Values used in typeflag field.  */
#define REGTYPE  '0'            /* regular file */
#define AREGTYPE '\0'           /* regular file */
#define LNKTYPE  '1'            /* link */
#define SYMTYPE  '2'            /* reserved */
#define CHRTYPE  '3'            /* character special */
#define BLKTYPE  '4'            /* block special */
#define DIRTYPE  '5'            /* directory */
#define FIFOTYPE '6'            /* FIFO special */
#define CONTTYPE '7'            /* reserved */

#define XHDTYPE  'x'            /* Extended header referring to the
                                   next file in the archive */
#define XGLTYPE  'g'            /* Global extended header */

/* Bits used in the mode field, values in octal.  */
#define TSUID    04000          /* set UID on execution */
#define TSGID    02000          /* set GID on execution */
#define TSVTX    01000          /* reserved */
                                /* file permissions */
#define TUREAD   00400          /* read by owner */
#define TUWRITE  00200          /* write by owner */
#define TUEXEC   00100          /* execute/search by owner */
#define TGREAD   00040          /* read by group */
#define TGWRITE  00020          /* write by group */
#define TGEXEC   00010          /* execute/search by group */
#define TOREAD   00004          /* read by other */
#define TOWRITE  00002          /* write by other */
#define TOEXEC   00001          /* execute/search by other */

/* Fin des pages Info de tar */


void afficherTar(struct posix_header ph);
long arrondi512(long n);

int main(int argc, char *argv[]) {
    int fd, lus;
    long size;
    struct posix_header ph;
    char strname[101];
    char strprefix[156];
    char fullname[257];
    char type;
    long mode;
    time_t mtime;
    struct tm *time;
    char strtime[1024];
    unsigned long checksum;
    unsigned char *p_cs;
    unsigned long i;
    int j;
    char bloc[512];

    if(argc > 1) {
        fd = open(argv[1], O_RDONLY);
        assert(fd != -1);
		lstar(fd,ph,size);		
	}
	 else {
      fprintf(stderr,"usage : ltar fichiertar\n");
      exit(1);
    }


    /* Il n’est nécessaire de fermer, exit le fera ; ça évite les
     * doutes sur la fermeture de STDIN_FILENO */
    /* close(fd); */
    return 0;
}


void lstar(int fd,struct posix_header ph, long size){
	

	// On a ouvert l'archive et on commence à lire le premier ellement	
	read(fd, &ph, sizeof(struct posix_header));
	// c'est pas jolie, mais quand on fait une fonction pour les droit 
	// on a un segment fault.
	char res[10]="";	
	int mode = strtol(ph.mode, NULL, 8);
		if(mode & TUREAD) strcat(res,"r");
		else strcat(res,"-");
		if(mode & TUWRITE) strcat(res,"w");
		else strcat(res,"-");
		if(mode & TUEXEC) strcat(res,"x");
		else strcat(res,"-");
	//
	if(mode & TGREAD) strcat(res,"r");
		else strcat(res,"-");
		if(mode & TGWRITE) strcat(res,"w");
		else strcat(res,"-");
		if(mode & TGEXEC) strcat(res,"x");
		else strcat(res,"-");
if(mode & TOREAD) strcat(res,"r");
		else strcat(res,"-");
		if(mode & TOWRITE) strcat(res,"w");
		else strcat(res,"-");
		if(mode & TGEXEC) strcat(res,"x");
		else strcat(res,"-");
		printf(ph.mtime);
	printf("%s %s/%s: %s  %li %s \n", res,ph.uname, ph.gname,ph.name,strtol(ph.size, NULL,8), ph.mtime);
	
	// On se déplace au prochaine elemeent (512 car TAR c'est des block de 512
	lseek(fd, (512-sizeof(struct posix_header)) + (arrondi512(size)) ,SEEK_CUR);
	//Et du coups on relis mais depuis notre nouvelle emplacement
	read(fd, &ph, sizeof(struct posix_header));
char res2[10]="";	
	 mode = strtol(ph.mode, NULL, 8);
		if(mode & TUREAD) strcat(res2,"r");
		else strcat(res2,"-");
		if(mode & TUWRITE) strcat(res2,"w");
		else strcat(res2,"-");
		if(mode & TUEXEC) strcat(res2,"x");
		else strcat(res2,"-");
	//
	if(mode & TGREAD) strcat(res2,"r");
		else strcat(res2,"-");
		if(mode & TGWRITE) strcat(res2,"w");
		else strcat(res2,"-");
		if(mode & TGEXEC) strcat(res2,"x");
		else strcat(res2,"-");
if(mode & TOREAD) strcat(res2,"r");
		else strcat(res2,"-");
		if(mode & TOWRITE) strcat(res2,"w");
		else strcat(res2,"-");
		if(mode & TGEXEC) strcat(res2,"x");
		else strcat(res2,"-");
		//time
		
		printf(ph.mtime);
		time_t t = ctime(&ph.mtime);
		char dft[] = "%D %H:%M";
		char *format = dft;

		struct tm lt;
		char buff[32];

		(void) localtime_r(&t, &lt);
strftime(res, sizeof(buff), format, &lt);
		
	printf("%s %s/%s: %s  %li %s \n", res2,ph.uname, ph.gname,ph.name,strtol(ph.size, NULL,8), res);

//ctime(&stStat.st_ctime)
}

/* Pour automatiser les droit
void jsp(char mode){

}
*/
long arrondi512(long n){

	while(n%512 != 0){
  	    n = n + 512 - (n%512) ;
	}


// utilise le ph.checksum pour la 24 !
return n;
}
