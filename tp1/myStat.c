#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

void afficherStat(struct stat stats);
void afficherDroit(struct stat stats);

int main(int argc, char *argv[]){
    struct stat stStat;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <cheminVersLeFichierVoulu>\n", argv[0]);
      	return -1;
    }
	if(stat(argv[1], &stStat) == 0){
		printf("Fichier : %s\n", argv[1]);
		afficherStat(stStat);
	}
	else{
		printf("Impossible d'acceder au fichier : %s\n", argv[1]);
	}
	return 0;
}

void afficherDroit(struct stat stStat){
   	printf( (S_ISDIR(stStat.st_mode)) ? "d" : "-");
    printf( (stStat.st_mode & S_IRUSR) ? "r" : "-");
    printf( (stStat.st_mode & S_IWUSR) ? "w" : "-");
    printf( (stStat.st_mode & S_IXUSR) ? "x" : "-");
    printf( (stStat.st_mode & S_IRGRP) ? "r" : "-");
    printf( (stStat.st_mode & S_IWGRP) ? "w" : "-");
    printf( (stStat.st_mode & S_IXGRP) ? "x" : "-");
    printf( (stStat.st_mode & S_IROTH) ? "r" : "-");
    printf( (stStat.st_mode & S_IWOTH) ? "w" : "-");
    printf( (stStat.st_mode & S_IXOTH) ? "x" : "-");
    printf(")");
}

void afficherStat(struct stat stStat){

	printf(" Taille du fichier : %lld	Blocs : %lld 	Blocs d'E/S : %ld \n",(long long) stStat.st_size, (long long) stStat.st_blocks, (long) stStat.st_blksize);
	printf("Inœud : %ld 		Lien : % ld \n", (long) stStat.st_ino, (long) stStat.st_nlink);
    printf("Accès : ( /");	afficherDroit(stStat); printf(" UID :  ( %ld/	) 	GID : ( %ld /	)\n",(long) stStat.st_uid, (long) stStat.st_gid);
	printf("Accès : %s", ctime(&stStat.st_atime));
	printf("Changt. :        %s", ctime(&stStat.st_mtime));
	printf("Changt. :        %s", ctime(&stStat.st_ctime));
}
