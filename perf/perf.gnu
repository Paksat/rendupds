set title "Performance de ./cat"
set logscale x
set xlabel "taille du buffer en octets"
set logscale y
set ylabel "temps d'exécution en secondes"
set style data linespoints
plot "perf.dat" using 1:2 title "temps d'exécution selon la taille du buffer"
pause -1  "Hit return to continue"
