#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main (int argc, char ** argv){
 	 unsigned char buffer[atoi(argv[1])];
	 int fd;
	 int fd2;
	 size_t fichierLu;
	 size_t fichierEcriture;

		// Pas assez d'arg
	if(argc != 3){
		perror ("Nombre d'arg insufisant la commande doit ressmebler à :./cat tailleDuBuffer fichier");
	 }

	if(buffer == NULL){
		perror("Impossible d'initialiser le buffer à vide");
	}
	
	fd = open(argv[2], O_RDONLY);
	if(fd == -1){
		perror ("L'acces au fichier damndé est impossible");
	 }
	  
	/* Permet d'envoyer le code dans le fichier /dev/dnull et pas le terminal car c'est moche*/
	fd2 = open("/dev/null", O_WRONLY);
	  if(fd2 == -1){
		perror ("impossible d'accéder au /dev/null");
	  }
		// Marche pas, fait dans le script !! printf("Lecture faite avec un buffer de : %s", argv[1]);

  	do{
	fichierLu = read(fd, buffer, sizeof(buffer));
		if(fichierLu==-1){
			perror("read");
		}
		fichierEcriture = write(fd2, buffer, fichierLu);
		if(fichierEcriture==-1){
			perror("write");
		}
	 }
	while(fichierLu !=0);

	// J'ai donc fini mon While, je peut finir fermé ma lecture j'en ai plus bessoin
	close (fd);
	return 0;
}
