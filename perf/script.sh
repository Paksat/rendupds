#!/bin/bash

# Je commence a effectuer ma commande cat avec un buffer de 1
# Et a chaque fois que la commande sera fini je recommencerais
# Mais avec une taille de buffer doublé.

#for((i = 1; i<=8388608 ;i=i*2)) --> Si on veut aussi le 8Mo octet ?
 
for((i = 1; i<=8000000;i=i*2)); do	
	# Affichage jolie decomenté la ligne suivante 
	 printf "Lecture faite avec un buffer de taille $i fait en : ";
  	/usr/bin/time -f '%e %U %S' ./cat "$i" Poly_AFSI_2020-21-Marc_DECALLONNE-Mambaye_LO.pdf
  	printf '\n'
done
